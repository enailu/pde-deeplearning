import json
from joblib import Parallel, delayed
from charateristics_utils import char_curve_values, char_curve_inv_values
from example_functions import initial_func1, initial_func2, initial_func3
import fenics
import pandas as pd
from functools import wraps
import sympy as sp
from sympy.abc import x
import numpy as np


def solve_burgers_pde(initial_func, num_elements, dt, T, nonlinear_func=None, **parameters):
    fenics.set_log_level(30)
    sympy_func = initial_func(**parameters, numpy=False, extended=False)
    x0 = sp.Symbol('x[0]')
    mesh = fenics.IntervalMesh(num_elements, 0, 1)
    V = fenics.FunctionSpace(mesh, 'P', 1)
    u = fenics.Function(V)
    v = fenics.TestFunction(V)
    ccode = sp.ccode(sympy_func.subs(x, x0), user_functions={'log': 'std::log'})
    u0 = fenics.Expression(ccode, element=V.ufl_element())
    u0 = fenics.project(u0, V)
    if nonlinear_func is None:
        def nonlinear_func(x):
            return x
    # implicit method
    F = u * v * fenics.dx + dt * fenics.grad(u)[0] * v * nonlinear_func(u) * fenics.dx - u0 * v * fenics.dx

    t_values = [0]
    t = dt

    u_evaluated = []
    for val in mesh.coordinates():
        u_evaluated.append(u0(val))

    u_over_time = [u_evaluated]
    coordinates = mesh.coordinates().copy()

    while t <= T:
        try:
            # fenics.solve(F == 0, u, solver_parameters={'nonlinear_solver':'snes'})
            fenics.solve(F == 0, u)
        except RuntimeError as re:
            print(parameters)
            print(re)
            raise RuntimeError
        t_values.append(t)
        u_evaluated = []
        for val in mesh.coordinates():
            u_evaluated.append(u(val))
        u_over_time.append(np.asarray(u_evaluated))
        t += dt
        u0.assign(u)
    return np.asarray(u_over_time), coordinates, t_values


with open('data/parameters1_exponent.json', 'r') as f:
    parameters1 = json.load(f)
with open('data/parameters2_exponent.json', 'r') as f:
    parameters2 = json.load(f)
with open('data/parameters3_exponent.json', 'r') as f:
    parameters3 = json.load(f)

dt = 0.001
num_x_vals = 1000

labels = ['solution', 'x_values', 't_values', 'characteristics_inverse', 'characteristics']


def inform_when_done(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        result = func(*args, **kwargs)
        print(f'Done executing {func.__name__} with parameters {args} and {kwargs}.')
        return result

    return wrapped


def solve(initial_func):
    @delayed
    @wraps(initial_func)
    # @inform_when_done
    def solver_function(exponent=None, **parameters):
        if exponent is not None:
            nonlinear_func = lambda x: x ** exponent
        else:
            nonlinear_func = None
        try:
            u, x_values, t_values = solve_burgers_pde(initial_func, num_x_vals, dt, 0.2, **parameters,
                                                      nonlinear_func=nonlinear_func)
        except RuntimeError:
            return tuple([None] * 5) + tuple([parameters[key] for key in sorted(parameters.keys())]) + (exponent,)
        characteristics = char_curve_values(initial_func(**parameters, numpy=True, extended=False), t_values, x_values)
        characteristics_inverse = char_curve_inv_values(initial_func(**parameters, numpy=True, extended=False),
                                                        t_values,
                                                        x_values[:-1], domain=[0, 1.0])
        return (u, x_values, t_values, characteristics_inverse, characteristics,
                *[parameters[key] for key in sorted(parameters.keys())], exponent)

    return solver_function


results = Parallel(n_jobs=-2, verbose=20)(
    solve(initial_func1)(exponent=exponent, a=a, b=b) for a, b, exponent in
    zip(parameters1['a'], parameters1['b'], parameters1['exponent']))
parameters1 = pd.DataFrame().from_records(results, columns=labels + ['a', 'b', 'exponent']).to_dict(orient='list')
np.save('data/parameters1_exponent_precise.npy', parameters1)

results = Parallel(n_jobs=4, verbose=20)(
    solve(initial_func2)(exponent=exponent, a=a, b=b) for a, b, exponent in
    zip(parameters2['a'], parameters2['b'], parameters2['exponent']))
parameters2 = pd.DataFrame().from_records(results, columns=labels + ['a', 'b', 'exponent']).to_dict(orient='list')
np.save('data/parameters2_exponent_precise.npy', parameters2)

results = Parallel(n_jobs=4, verbose=20)(
    solve(initial_func3)(exponent=exponent, a=a, b=b, c=c) for a, b, c, exponent in
    zip(parameters3['a'], parameters3['b'], parameters3['c'], parameters3['exponent']))
parameters3 = pd.DataFrame().from_records(results, columns=labels + ['a', 'b', 'c', 'exponent']).to_dict(orient='list')
np.save('data/parameters3_exponent_precise.npy', parameters3)
