from nn_architectures import PdeRNN
from generator import DataGenerator
import sys
from tensorflow.python.keras import optimizers
import numpy as np
import matplotlib.pyplot as plt
import logging
import tensorflow as tf

tf.get_logger().addFilter(lambda record: "\"v_field\" missing from loss dictionary" not in record.getMessage())
tf.get_logger().addFilter(lambda record: "\"initial_phi\" missing from loss dictionary" not in record.getMessage())
tf.get_logger().addFilter(lambda record: "colocate_with" not in record.getMessage())
if len(sys.argv) > 1:
    fit = sys.argv[1] == '-fit'
else:
    fit = False

data_gen = DataGenerator('data/single_files/single_files_2/',
                         parameters=[['a', 'b'], 't_values', 'dt_u', 'dx_u', 'x_values'], t_stride=3000 // 200,
                         batch_size=1000)
num_t_vals = data_gen.num_used_t_vals
dt = data_gen.dt
dx = data_gen.dx

pde_rnn = PdeRNN(num_t_vals, parameter_units=200, input_units=60, dt=dt, dx=dx, num_parameters=2)
pde_rnn.build(activation='relu')
model = pde_rnn.model_w_loss

if fit:
    model.compile(optimizer=optimizers.Adam(lr=0.001))
    # shuffle = False because it is done in custom generator
    history = model.fit_generator(data_gen,
                                  epochs=500,
                                  verbose=1, shuffle=False)
    model.save_weights('results/withab_gen/weights.h5')
    np.save('results/withab_gen/history.npy', history.history)
else:
    model.load_weights('results/withab_gen/weights.h5')
    # pde_rnn.evaluate(30, dataset_new)
    # history = np.load('results/withab_gen/history.npy').item()
    # plt.figure()
    # plt.plot(np.arange(len(history['loss']))[::20], history['loss'][::20], label='loss')
    # plt.plot(np.arange(len(history['val_loss']))[::20], history['val_loss'][::20], label='val_loss')
    # plt.legend()
    # plt.show()
