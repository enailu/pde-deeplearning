import pandas as pd
import numpy as np

df = pd.DataFrame(np.load('data/parameters1_precise.npy').item())
df = df[['solution', 'characteristics_inverse', 'characteristics', 'a', 'b']]
# df = pd.read_hdf('data/dataset_complete.h5', mode='r')
# for i, row in df.iterrows():
#     row.to_json('data/single_files/data_{:04d}.json'.format(i), orient='records')
df_split = np.array_split(df, len(df.index))
for i in range(len(df_split)):
    df_split[i].to_json('data/single_files/single_files_1/data_{:04d}.json'.format(i), orient='records')
