import numpy as np
import sympy as sp
from sympy.abc import x, y
import matplotlib.pyplot as plt
from pynverse import inversefunc

def char_curve_in_t(f, x0, nonlinear_func=None):
    if nonlinear_func is None:
        return lambda t: x0 + f(x0) * t
    else:
        return lambda t: x0 + nonlinear_func(f(x0))


def char_curve_in_t_inv(f, x0, nonlinear_func=None):
    def fun(x):
        if nonlinear_func is None:
            y = (x - x0) / f(x0)
        else:
            y = (x - x0) / nonlinear_func(f(x0))
        return y

    if f(x0) != 0:
        return fun
    else:
        return None


def char_curve_values(f, t_values, x_values, nonlinear_func=None):
    x_values = np.expand_dims(np.squeeze(x_values), 0)
    t_values = np.expand_dims(np.squeeze(t_values), 1)
    if nonlinear_func is None:
        return x_values + f(x_values) * t_values
    else:
        return x_values + nonlinear_func(f(x_values)) * t_values


def char_curve_in_x0(f, t, nonlinear_func=None):
    if nonlinear_func is None:
        return lambda x0: x0 + f(x0) * t
    else:
        return lambda x0: x0 + nonlinear_func(f(x0)) * t


def char_curve_in_x0_inv(f, t, nonlinear_func=None, domain=None):
    return inversefunc(char_curve_in_x0(f, t, nonlinear_func), domain=domain, open_domain=[False, True])


def char_curve_inv_values(f, t_values, y_values, nonlinear_func=None, domain=None):
    result = []
    for t in t_values:
        inv_func_at_t = char_curve_in_x0_inv(f, t, nonlinear_func, domain)
        result.append(inv_func_at_t(y_values))
    return np.squeeze(np.asarray(result))


def char_curve_in_x0_sympy(f, t, nonlinear_func=None):
    if nonlinear_func is None:
        return y + f.subs(x, y) * t
    else:
        return y + nonlinear_func.subs(x, f.subs(x, y)) * t


def char_curve_in_x0_sympy_inv(f, t, x, nonlinear_func=None):
    val = sp.solve(char_curve_in_x0_sympy(f, t, nonlinear_func) - x)
    # print(val)
    return val


def char_curve_in_x0_sympy_inv2(f_no_piecewise, t_val, left, right, nonlinear_func=None):
    vals_func = sp.solve(char_curve_in_x0_sympy(f_no_piecewise, sp.Symbol('t'), nonlinear_func) - sp.Symbol('s'),
                         exclude=['s', 't'],
                         rational=False)
    delta = 30 * np.finfo(np.float64).resolution

    def inv(x_values, vals_func):
        vals = [val_func(x_values) for val_func in vals_func]
        vals = list(
            map(lambda x: np.where(np.logical_and(x >= left - delta, x <= right + delta), x, np.nan), vals))
        vals.append(
            np.where(np.logical_or(x_values >= right - delta, x_values <= left + delta), x_values, np.nan))
        vals = np.asarray(vals)
        return vals

    return lambda x_values: inv(x_values, [sp.lambdify('s', val.subs(sp.Symbol('t'), t_val)) for val in vals_func])


def plot_char_curves(f, x0_values, x_values, max_t=None, nonlinear_func=None):
    plt.figure()
    for x0 in x0_values:
        curve = char_curve_in_t_inv(f, x0, nonlinear_func)
        if curve is not None:
            plt.plot(x_values, curve(x_values), color='gray')
        else:
            plt.axvline(x0, color='gray')
    if max_t is not None:
        plt.ylim(top=max_t)
    plt.ylim(bottom=0)
    plt.xlabel('x')
    plt.ylabel('t')
    # plt.show()


def plot_char_curves_x0(f, t_values, x0_values, nonlinear_func=None):
    plt.figure()
    for t in t_values:
        curve = char_curve_in_x0(f, t, nonlinear_func)
        plt.plot(x0_values, curve(x0_values), label=t)
    plt.show()
    plt.xlabel('x0')
    plt.ylabel('x')
    plt.legend()


def breaking_time(f, left, right, nonlinear_func=None, solve_numerically=True):
    if nonlinear_func is None:
        fx = sp.diff(f, x)
        fxx = sp.diff(fx, x)
    else:
        fx = sp.diff(f.subs(x, nonlinear_func), x)
        fxx = sp.diff(fx, x)
    succeeded = False
    if not solve_numerically:
        try:
            potential_breaking_points = sp.solve(fxx, x)
            succeeded = True
        except NotImplementedError:
            pass
        except():
            potential_breaking_points = []
            succeeded = True
    if not succeeded or solve_numerically:
        potential_breaking_points = []
        for initial_guess in np.linspace(left, right):
            try:
                potential_breaking_points += [sp.nsolve(fxx, initial_guess)]
            except (ZeroDivisionError, ValueError, KeyError):
                continue
    potential_breaking_points = list(filter(lambda x: x.is_real and left <= x <= right, set(potential_breaking_points)))
    potential_breaking_points += [left, right]
    potential_breaking_times = filter(lambda x: x >= 0,
                                      filter(lambda x: x.is_finite and x.is_real, map(lambda y: -1 / fx.subs(x, y),
                                                                                      potential_breaking_points)))
    potential_breaking_times = list(map(float, potential_breaking_times))
    if len(potential_breaking_times) > 0:
        return min(potential_breaking_times)
    else:
        return None
