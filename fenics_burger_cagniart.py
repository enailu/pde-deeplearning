from fenics import *
import matplotlib.pyplot as plt
import numpy as np

t0 = 0
num_elements = 500
p_lbd = 1.3
p_nu = 4
p_eps = 0.04
dt = 0.0001


class PeriodicBoundary(SubDomain):

    def inside(self, x, on_boundary):
        return bool(-DOLFIN_EPS -1 < x[0] < -1 + DOLFIN_EPS and on_boundary)

    def map(self, x, y):
        y[0] = x[0] - 2
        print(x)


mesh = IntervalMesh(num_elements, -1, 1)
V = FunctionSpace(mesh, 'P', 1, constrained_domain=PeriodicBoundary())
# V = FunctionSpace(mesh, 'P', 1)

u = Function(V)
v = TestFunction(V)

u0 = Expression("p_lbd - sin(DOLFIN_PI*(x[0]))", p_lbd=p_lbd, element=V.ufl_element())
# u0 = Expression("-DOLFIN_EPS < x[0] < DOLFIN_EPS ? 2 : 1", element=V.ufl_element())
u0 = project(u0, V)

F = u*v*dx + dt*p_nu*grad(u)[0]*v*u*dx + dt*p_eps*inner(grad(u), grad(v))*dx - u0*v*dx
#F = u*v*dx + dt*p_nu*grad(u)[0]*v*u*dx - u0*v*dx

T = 0.1
t = dt

u_over_time=[]

while t <= T:
    solve(F == 0, u)
    t += dt
    u0.assign(u)
    u_evaluated = []
    for val in mesh.coordinates():
        u_evaluated.append(u(val))
    u_over_time.append(np.asarray(u_evaluated))

plt.plot(np.asarray(u_over_time).T[:, :400:10])
plt.show()