import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.python.keras import optimizers

import plotting_utils
from nn_architectures import PdeRNN, myround
from periodic_data_utils import dx_derivative, dt_derivative

# feed u(x,t) instead of u0(x)
tf.compat.v1.disable_eager_execution()

if len(sys.argv) > 1:
    fit = sys.argv[1] == '-fit'
else:
    fit = False

file_suffix = 'precise'

dataset1 = pd.DataFrame(np.load('data/parameters1_precise.npy').item())
dataset2 = pd.DataFrame(np.load('data/parameters2_precise.npy').item())
dataset3 = pd.DataFrame(np.load('data/parameters3_precise.npy').item()).drop('c', 1)
# dataset = pd.concat([dataset, dataset2, dataset3]).to_dict(orient='list')
dataset = pd.concat([dataset1, dataset3, dataset2]).to_dict(orient='list')


num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
t_vals = np.asarray(dataset['t_values'][0])
dx = dataset['x_values'][0][1] - dataset['x_values'][0][0]
dt = dataset['t_values'][0][1] - dataset['t_values'][0][0]

dt_u = [np.expand_dims(dt_derivative(u, dt).T, -1) for u in dataset['solution']]
dx_u = [np.expand_dims(dx_derivative(u, dx).T, -1) for u in dataset['solution']]
t_values = [np.broadcast_to(np.asarray(t_value)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1))
            for t_value in dataset['t_values']]
initial_functions = [np.asarray(u[0]) for u in dataset['solution']]
# solution = [np.concatenate((np.ones(shape=(num_x_vals, 1, 1)), np.asarray(solution).T[:, :, np.newaxis]), axis=1)[:, :-1, :] for
#        solution in
#        dataset['solution']]
solution = [np.asarray(solution).T[:, :, np.newaxis] for solution in dataset['solution']]
dataset_new = {'dt_u': dt_u,
               'dx_u': dx_u,
               't_values': t_values,
               'parameters_depending_on_t': solution,
               'characteristics_inverse': dataset['characteristics_inverse'],
               'x_values': dataset['x_values'],
               'initial_functions': initial_functions}

# convert to dataframe for sklearn support
dataset_df = pd.DataFrame(dataset_new)
# split rows into two
train, test = train_test_split(dataset_df, test_size=0.1, random_state=10, shuffle=True)
# convert back to dict
train = train.to_dict(orient='list')
test = test.to_dict(orient='list')

for key, value in train.items():
    if key is not 'characteristics_inverse':
        train[key] = np.concatenate(value, axis=0)

for key, value in test.items():
    if key is not 'characteristics_inverse':
        test[key] = np.concatenate(value, axis=0)

# plot_train_function(0.5, train, t_range=slice(10, None, None))
# sanity check

# # array of shape (num_velocities*num_xvals, num_t_vals, 1) from (num_velocities, num_xvals, num_tvals)
# dx_u_test = np.stack(test['dx_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# dt_u_test = np.stack(test['dt_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# velocity_test = np.stack(test['velocity'].values).reshape((-1, 1))
#
# u_train = np.stack(train['u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# dx_u_train = np.stack(train['dx_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# dt_u_train = np.stack(train['dt_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# velocity_train = np.stack(train['velocity'].values).reshape((-1, 1))
#
#

for key, value in train.items():
    print(str(key) + ": " + str(len(value)) + " entries of shape " + str(np.asarray(value[0]).shape))

pde_rnn = PdeRNN(num_t_vals, parameter_units=200, input_units=60, dt=dt, dx=dx, num_parameters=0,
                 num_parameters_depending_on_t=1)
pde_rnn.build(activation='relu')
model = pde_rnn.model_w_loss

if fit:
    model.compile(optimizer=optimizers.Adam(lr=0.001))
    history = model.fit(
        [train['parameters_depending_on_t'],
         train['t_values'],
         train['dt_u'],
         train['dx_u'],
         train['x_values']],
        validation_data=(
            [test['parameters_depending_on_t'],
             test['t_values'],
             test['dt_u'],
             test['dx_u'],
             test['x_values']], None),
        batch_size=num_x_vals,
        epochs=200,
        shuffle=True,
        verbose=2)
    model.save_weights('results/withutx/weights_{}.h5'.format(file_suffix))
    np.save('results/withutx/history_{}.npy'.format(file_suffix), history.history)
else:
    model.load_weights('results/withutx/weights_{}.h5'.format(file_suffix))
    pde_rnn.evaluate(70, dataset_new)
    history = np.load('results/withutx/history_{}.npy'.format(file_suffix)).item()
    plt.figure()
    step_ticks = myround(len(history['loss']) // 10)
    step = step_ticks // 2
    plt.plot(np.arange(len(history['loss']))[::step], history['loss'][::step], label='loss')
    plt.plot(np.arange(len(history['val_loss']))[::step], history['val_loss'][::step], label='val_loss')
    plt.xticks(np.arange(len(history['loss']))[::step_ticks])
    plt.legend()
    plt.show()
    # out = pde_rnn.predict_nn_function(np.linspace(0, 2, 200), 0, t_vals)
    # pde_rnn.plot_nn_function(np.linspace(0, 2, 200), 0.5, t_vals)
    v_field = pde_rnn.evaluate_v_field(70, dataset_new)


    def true_v_field(i, dataset):
        initial_function = dataset['initial_functions'][i]
        t_values = dataset['t_values'][i]
        initial_function_derivative = dx_derivative(initial_function[np.newaxis, :], dx).T
        return -initial_function[:, np.newaxis] / (1 + t_values[:, :, 0] * initial_function_derivative)


    true_v_field_values = true_v_field(70, dataset_new)
    plt.figure()
    plt.title('True vector field')
    plotting_utils.plots(true_v_field_values)
    plt.figure()
    plt.title('Learned vector field')
    plotting_utils.plots(v_field)
