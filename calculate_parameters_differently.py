from example_functions import initial_func3
import numpy as np
from periodic_data_utils import dx_derivative
import matplotlib.pyplot as plt
import json

good_values3 = {'a': [],
                'b': [],
                'c': [],
                'bt': []}

for b in [1, 2]:
    for a in np.arange(2, 20, 2):
        for c in np.arange(a, 20, 2):
            u0 = initial_func3(a, b, c, numpy=True)(np.linspace(0, 1, 1000))
            derivatives = dx_derivative(u0[np.newaxis, :], 1. / 1000)
            bt = - 1. / np.min(derivatives)
            if bt > 0.20:
                good_values3['a'].append(float(a))
                good_values3['b'].append(float(b))
                good_values3['c'].append(float(c))
                good_values3['bt'].append(bt)
            # plt.plot(u0, label=round(bt, 2))
            # plt.legend()
        # plt.show(block=True)
        # plt.clf()

with open('data/parameters3_differently.json', 'w') as f:
    json.dump(good_values3, f)
