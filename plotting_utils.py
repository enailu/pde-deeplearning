import matplotlib.pyplot as plt
import numpy as np


class DatasetPlotter:

    def __init__(self, dataset):
        """
        :param dataset: Dataset contains a list of data for each key. This data is of shape (num_t_vals, num_x_vals),
        or a list with num_t_vals elements.
        """
        self.dataset = dataset

    def plots(self, i, key='sol', t_slice=None, x_slice=None, inverse=False, block=False):
        """
        Plot the data of the specified key and index of the dataset.
        Dataset contains a list of data for each key. This data is of shape (num_t_vals, num_x_vals), or a list with
        num_t_vals elements.
        :param i: index
        :param key: key of dataset
        :param t_slice: slice or index of t_values to select
        :param x_slice: slice or index of x_values to select
        :param inverse: boolean, plot instead the inverse of the data
        :param block: boolean, block when showing plot
        """
        x_values = self.dataset['x_values'][i]
        y_values = np.asarray(self.dataset[key][i]).T
        plots(y_values, x_slice=x_slice, list_slice=t_slice, x_values=x_values, inverse=inverse, block=block)


def plots(y_values, x_slice=None, list_slice=slice(None, None, 20), x_values=None, inverse=False, block=False,
          color=None
          ):
    """
    Plot y_values.
    :param y_values: array of shape (num_x_vals, n) or list
    :param x_slice: slice or index of x_values to select
    :param list_slice: slice or index of list elements to select
    :param x_values: custom x_values
    :param inverse: plot instead the inverse of the data
    :param block: block when showing plot
    """
    if isinstance(y_values, list):
        y_values = np.squeeze(np.asarray(y_values)).T
    if x_values is None:
        x_values = range(y_values.shape[0])
    if x_slice is not None:
        x_values = x_values[x_slice]
    if list_slice is not None:
        y_values = y_values[:, list_slice]
    if x_slice is not None:
        y_values = y_values[x_slice]
    if not inverse:
        if color is not None:
            plt.plot(x_values, y_values, color=color)
        else:
            plt.plot(x_values, y_values)
    else:
        if color is not None:
            plt.plot(y_values, x_values, color=color)
        else:
            plt.plot(y_values, x_values)
    plt.show(block=block)
