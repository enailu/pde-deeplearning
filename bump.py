import numpy as np
import time
from periodic_data_utils import extend_function, extend_function_sympy
from charateristics_utils import breaking_time, char_curve_in_x0_sympy_inv, char_curve_in_x0_sympy_inv2, \
    plot_char_curves
import matplotlib.pyplot as plt
import sympy
from sympy.abc import x
import testing_utils


def solution(f, t, x_values, nonlinear_func=None, breaking_time=1):
    f_np = sympy.lambdify(x, f, "numpy")
    if t < breaking_time:
        new_x_values = [float(char_curve_in_x0_sympy_inv(f, t, x_val, nonlinear_func)[0]) for x_val in x_values]
        # return [float(f.subs(x, char_curve_in_x0_sympy_inv(f, t, x_val)[0])) for x_val in x_values]
        return f_np(np.asarray(new_x_values))
    else:
        return None

def solution2(f, f_no_piecewise, t, x_values, left, right, nonlinear_func=None, s=None):
    f_np = sympy.lambdify(x, f, "numpy")
    delta = np.finfo(np.float64).resolution
    new_x_values = char_curve_in_x0_sympy_inv2(f_no_piecewise, t, left, right, nonlinear_func)(x_values)
    if s is None:
        s = np.inf
    # new_x_values = list(map(lambda x: x + [0] * (3 - len(x)), new_x_values))
    # return [float(f.subs(x, char_curve_in_x0_sympy_inv(f, t, x_val)[0])) for x_val in x_values]
    # return f_np(np.asarray(new_x_values))
    def choice_func(args):
        x_value, new_x_values = args
        if x_value <= s + delta:
            return new_x_values[0]
        else:
            return new_x_values[2]

    y_values = np.asarray(list(map(choice_func, zip(x_values, new_x_values.T))))
    assert np.all(np.isreal(y_values))
    y_values = np.real(y_values)
    if np.any(np.isnan(y_values)):
        print(np.where(np.isnan(y_values)))
    return f_np(y_values)


def breaking_curve(f, f_piecewise, breaking_point, breaking_time, t_values, left, right):
    delta = np.finfo(np.float64).resolution
    t_values = t_values[t_values > breaking_time]
    s1 = [breaking_point]
    s2 = [breaking_point]
    s3 = [breaking_point]
    for t_value in t_values:
        breaking_point2 = (4 * t_value ** 2 + 4 * t_value + 1) / (4 * t_value)
        dx = (breaking_point2 - breaking_point) / 2
        x_values = np.asarray([breaking_point, breaking_point + dx])
        first_values = char_curve_in_x0_sympy_inv2(f, t_value, left, right)(x_values)
        y_values = f_piecewise(first_values[:, 1])
        s1.append(s1[-1] + (y_values[0] + y_values[1]) * 1 / 2 * (t_values[1] - t_values[0]))
        s2.append(s2[-1] + (y_values[1] + y_values[2]) * 1 / 2 * (t_values[1] - t_values[0]))
        s3.append(s3[-1] + (y_values[0] + y_values[2]) * 1 / 2 * (t_values[1] - t_values[0]))
    print('')
    return s1, s2, s3


def main():
    # x_values = np.linspace(0, 3, 200)
    dx = 0.02
    x_values = np.arange(0, 3, dx)
    x0_values = np.concatenate((np.linspace(2, 4, 10), np.linspace(0, 1, 20), np.linspace(1, 2, 10)))

    # bump_func = extend_function(lambda x: 1 - (x - 1) ** 2, 0, 2)

    square_func_sympy = 2 * (1 - (x - 1) ** 2)
    # has the advantage of zero derivative at 0 and 2
    quartic_func_sympy = x ** 2 * (x - 2) ** 2

    func = square_func_sympy
    bump_func_sympy = extend_function_sympy(func, x, 0, 2)
    bump_func_numpy = sympy.lambdify(x, bump_func_sympy, "numpy")

    nonlinear_func_sympy = x
    nonlinear_func_numpy = sympy.lambdify(x, nonlinear_func_sympy, 'numpy')

    breaking_time_2 = breaking_time(func, 0, 2, nonlinear_func_sympy, solve_numerically=False)

    # plot_char_curves_x0(bump_func, t_values=np.linspace(0, 1, 5), x0_values=x_values)
    # plt.axhline(y=2)

    # plot_char_curves(bump_func_numpy, x0_values, x_values, max_t=2, nonlinear_func=nonlinear_func_numpy)

    dt = 0.005
    t_values = np.arange(0, 2, dt)
    s1, s2, s3 = breaking_curve(func, bump_func_numpy, 2, breaking_time_2, t_values, 0, 2)
    # plt.plot(s1, np.append(breaking_time, t_values[t_values > breaking_time]), label='s1')
    # plt.plot(s2,  np.append(breaking_time, t_values[t_values > breaking_time]), label='s2')
    # plt.plot(s3,  np.append(breaking_time, t_values[t_values > breaking_time]), label='s3')
    # plt.legend()
    # plt.show()

    s1 = [np.inf] * (len(t_values) - len(s1)) + s1
    s2 = [np.inf] * (len(t_values) - len(s2)) + s2
    s3 = [np.inf] * (len(t_values) - len(s3)) + s3

    u0 = solution(bump_func_sympy, 0, x_values)
    u1 = solution(bump_func_sympy, 0.1, x_values)

    start_time = time.time()
    # ut1 = solution2(bump_func_sympy, func, t_values[10], x_values, s2[10], 0, 2)
    # ut2 = solution2(bump_func_sympy, func, t_values[15], x_values, s2[15], 0, 2)
    # ut3 = solution2(bump_func_sympy, func, t_values[16], x_values, s2[16], 0, 2)

    # first_curve = [values[0] for values in ut]
    # second_curve = [values[1] for values in ut if len(values) > 1]
    # third_curve = [values[2] if len(values) > 2 else np.inf for values in ut]
    time_elapsed = time.time() - start_time
    # plt.figure()
    # plt.plot(x_values, ut)
    plt.figure()
    plt.plot(x_values, bump_func_numpy(x_values), label=t_values[0])
    # plt.plot(x_values, ut1)
    # plt.plot(x_values, ut2)
    # plt.plot(x_values, ut3)
    # plt.show()

    # diffs = []
    # # for s in [s1, s2, s3]:
    # ut2 = solution2(bump_func_sympy, func, t_values[1], x_values, 0, 2, nonlinear_func_sympy)
    # ut3 = solution2(bump_func_sympy, func, t_values[2], x_values, 0, 2, nonlinear_func_sympy)
    # # plt.figure()
    # plt.plot(x_values, ut2, label=t_values[1])
    # plt.plot(x_values, ut3, label=t_values[2])
    # #plt.show()
    # plt.legend()
    # diff = testing_utils.test_hyperbolic_wave(np.asarray([ut2, ut3]), dx=dx, dt=dt, nonlinear_func=nonlinear_func_numpy)
    # diffs.append(np.abs(diff[0]))
    return u0, u1


x_values, dx = np.linspace(0, 2, 800, retstep=True)
square_func_sympy = 2 * (1 - (x - 1) ** 2)
dt = 0.001
t_values = np.arange(0, 0.22, dt)[1:]
u = []
for t in t_values:
    ut = solution2(square_func_sympy, square_func_sympy, t, x_values, 0, 2)
    u.append(ut)
u = np.asarray(u)
diff = testing_utils.test_hyperbolic_wave(u, dx=dx, dt=dt)
