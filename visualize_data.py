from pymor.gui.visualizers import OnedVisualizer
from pymor.basic import OnedGrid
from pymor.vectorarrays.numpy import NumpyVectorArray, NumpyVectorSpace
import json
import numpy as np

min_bt_index = 0
min_bt = 0.5
for i in range(88, 132):
    with open('data/single_files/single_files_2/data_{:04d}.json'.format(i)) as f:
        data = json.load(f)
        if data['bt'] < min_bt:
            min_bt = data['bt']
            min_bt_index = i
        print(i)

with open('data/single_files/single_files_2/data_{:04d}.json'.format(min_bt_index)) as f:
    data = json.load(f)

grid = OnedGrid(num_intervals=int(1 // data['dx'] + 1), identify_left_right=True)
data = NumpyVectorArray(np.asarray(data['solution']), NumpyVectorSpace(dim=len(data['solution'])))
OnedVisualizer(grid, 0).visualize(data, d=None)
