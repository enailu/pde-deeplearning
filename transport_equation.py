import numpy as np
from periodic_data_utils import periodic_coordinates, extend_function, generate_solutions_to_transport_equation
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.keras.layers import Dense
import pandas as pd
from collections import defaultdict
from sklearn.model_selection import train_test_split
from grid_utils import find_closest_indices

dx = 0.01
dt = 0.001
end_x = 3
x_vals = np.arange(0, end_x, dx)
t_vals = np.arange(0, 0.01, dt)
num_x_vals = len(x_vals)
num_t_vals = len(t_vals)


def f(x, a=2, b=2):
    return np.sin(a * np.pi * x) * (np.cos(2 * np.pi * x) - 1) ** b
    # return (np.cos(2 * np.pi * x) - 1)**2


# sanity check
def dx_f_fun(x, a=2, b=2):
    return a * np.pi * (np.cos(2 * np.pi * x) - 1) ** b - np.sin(a * np.pi * x) * b * (np.cos(2 * np.pi * x) - 1) ** (
                b - 1) * np.sin(2 * np.pi * x) * 2 * np.pi
    #return -4 * np.pi * np.sin(2 * np.pi * x) * (np.cos(2 * np.pi * x) - 1)


def generate_data(x_vals, t_vals, velocity=1, wrap=1, a=2, b=2):
    xv, tv = np.meshgrid(x_vals, t_vals)
    coordinates = periodic_coordinates(xv - velocity * tv, wrap)
    f_extended = extend_function(f, 0, 1)
    data = f_extended(coordinates, a, b)
    return data


dataset = generate_solutions_to_transport_equation(f, x_vals, t_vals, dt, dx, wrap=3, a=[2], b=[2],
                                                   velocity=np.arange(1, 10, 0.2))

dataset_df = pd.DataFrame(dataset)

train, test = train_test_split(dataset_df, test_size=0.33, random_state=42, shuffle=True)

# sanity check
diff = []
xv, tv = np.meshgrid(x_vals, t_vals)
for i in range(6):
    dt_u = dataset['dt_u'][i][1:-1, :]
    dx_u = dataset['dx_u'][i][1:-1, :]
    u = dataset['u'][i]
    v = dataset['velocity'][i]
    a = dataset['a'][i]
    b = dataset['b'][i]
    coordinates = periodic_coordinates(xv - v * tv, 3)
    dx_f = extend_function(dx_f_fun, 0, 1)(coordinates, a, b)
    diff.append(np.abs(dt_u + v * dx_u))
    if np.max(np.average(diff)) > 0.1:
        print("WARNING: " + np.average(diff))

# start to define neural network
input_dx_u = tf.placeholder(dtype='float', shape=(num_t_vals, num_x_vals, 1))
input_dt_u = tf.placeholder(dtype='float', shape=(num_t_vals, num_x_vals, 1))
input_velocity = tf.placeholder(dtype='float', shape=(1, 1))
input_x = tf.constant(value=x_vals, dtype='float', shape=(num_x_vals, 1))
# var_c = tf.Variable(initial_value=0, trainable=True, name='c', dtype='float')

phi_xt = input_x
phi_values = [phi_xt]
loss = 0

layer = Dense(10, activation='relu', use_bias=True)(input_velocity)
var_c = Dense(1, activation=None, kernel_initializer=tf.keras.initializers.Constant(1), use_bias=True)(layer)

# peridocitiy not handled in values of phi
for i in range(1, num_t_vals):
    phi_xt = phi_xt + dt * var_c
    phi_values.append(phi_xt)
    loss += tf.reduce_sum((input_dt_u[i] + var_c * input_dx_u[i]) ** 2) * dx * dt

opt = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

# training loop
with tf.Session() as sess:
    tf.global_variables_initializer().run()
    nb_epochs = 1000
    c_vals = np.zeros((nb_epochs, 1))
    current_c = 0
    losses = []
    test_losses = []
    test_feed_dicts = [{input_dx_u: np.expand_dims(test['dx_u'].iloc[j], -1),
                        input_dt_u: np.expand_dims(test['dt_u'].iloc[j], -1),
                        input_velocity: np.expand_dims(test['velocity'].iloc[j], -1)
                        } for j in range(len(test['u']))]
    for i in range(nb_epochs):
        epoch_loss = 0
        for j in np.random.permutation(len(train['u'])):
            feed_dict = {input_dx_u: np.expand_dims(train['dx_u'].iloc[j], -1),
                         input_dt_u: np.expand_dims(train['dt_u'].iloc[j], -1),
                         input_velocity: np.expand_dims(train['velocity'].iloc[j], -1)
                         }
            current_loss, current_c, _ = sess.run([loss, var_c, opt], feed_dict=feed_dict)
            epoch_loss += current_loss
            # print("Loss:" + str(current_loss))
            # print("C:" + str(current_c))
        c_vals[i] = current_c
        losses.append(epoch_loss / len(dataset['u']))
        test_loss = 0
        for k in range(len(test_feed_dicts)):
            test_loss += sess.run(loss, feed_dict=test_feed_dicts[k])
        test_losses.append(test_loss / len(test_feed_dicts))
        # print(current_c)
    test['velocity'] = test['velocity'].astype(float)
    u_values_by_velocity = dict(test.groupby('velocity')['u'].apply(list))
    velocities = u_values_by_velocity.keys()
    phi_dict = defaultdict(np.ndarray)
    w_dict = defaultdict(list)
    for velocity in velocities:
        phi = np.squeeze(sess.run(phi_values, feed_dict={input_velocity: np.array(velocity, ndmin=2)}))
        phi_dict[velocity] = phi
        for u in u_values_by_velocity[velocity]:
            phi_indices, out_of_bounds, _ = find_closest_indices(x_vals, phi, dx)
            t_indices = np.repeat(np.expand_dims(np.arange(num_t_vals), axis=1), num_x_vals, 1)
            if not np.allclose(u[out_of_bounds], 0):
                continue
            w_dict[velocity].append(u[t_indices, phi_indices])

# plt.plot(np.arange(nb_epochs), c_vals)

# plt.ylim(top=10)
plt.plot(losses[20:], label='loss')
plt.plot(test_losses[20:], label='test loss')
plt.legend()
plt.show()

any_key = max(velocities)
plt.title("After alignment")
plt.plot(w_dict[any_key][0].T)
plt.show()
plt.title("Before alignment")
plt.plot(u_values_by_velocity[any_key][0].T)
plt.show()
