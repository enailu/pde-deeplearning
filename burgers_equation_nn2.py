import sys

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.python.keras import optimizers

from nn_architectures import PdeRNN
from periodic_data_utils import dx_derivative, dt_derivative

file_suffix = 'leave_one_out'
tf.compat.v1.disable_eager_execution()

if len(sys.argv) > 1:
    fit = sys.argv[1] == '-fit'
else:
    fit = False

dataset = pd.DataFrame(np.load('data/parameters1.npy').item())
dataset2 = pd.DataFrame(np.load('data/parameters2.npy').item())
dataset3 = pd.DataFrame(np.load('data/parameters3_old.npy').item()).drop('c', 1)
dataset = pd.concat([dataset, dataset2, dataset3]).to_dict(orient='list')


num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
t_vals = np.asarray(dataset['t_values'][0])
dx = dataset['x_values'][0][1] - dataset['x_values'][0][0]
dt = dataset['t_values'][0][1] - dataset['t_values'][0][0]

dt_u = [np.expand_dims(dt_derivative(u, dt).T, -1) for u in dataset['sol']]
dx_u = [np.expand_dims(dx_derivative(u, dx).T, -1) for u in dataset['sol']]
t_values = [np.broadcast_to(np.asarray(t_value)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1))
            for t_value in dataset['t_values']]
initial_functions = [np.asarray(u[0]) for u in dataset['sol']]
parameters = [np.concatenate([np.broadcast_to(a, (num_x_vals, 1)), np.broadcast_to(b, (num_x_vals, 1))], axis=-1)
              for a, b in zip(dataset['a'], dataset['b'])]
dataset_new = {'dt_u': dt_u,
               'dx_u': dx_u,
               't_values': t_values,
               'parameters': initial_functions,
               'characteristics_inv': dataset['characteristics_inv'],
               'x_values': dataset['x_values'],
               'initial_functions': initial_functions}

# convert to dataframe for sklearn support
dataset_df = pd.DataFrame(dataset_new)
# split rows into two
train, test = train_test_split(dataset_df, test_size=100, random_state=10, shuffle=False)
# convert back to dict
train = train.to_dict(orient='list')
test = test.to_dict(orient='list')

train_dict = train.copy()
test_dict = test.copy()

for key, value in train.items():
    if key is not 'characteristics_inv':
        train[key] = np.concatenate(value, axis=0)

for key, value in test.items():
    if key is not 'characteristics_inv':
        test[key] = np.concatenate(value, axis=0)

# plot_train_function(0.5, train, t_range=slice(10, None, None))
# sanity check

# # array of shape (num_velocities*num_xvals, num_t_vals, 1) from (num_velocities, num_xvals, num_tvals)
# dx_u_test = np.stack(test['dx_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# dt_u_test = np.stack(test['dt_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# velocity_test = np.stack(test['velocity'].values).reshape((-1, 1))
#
# u_train = np.stack(train['u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# dx_u_train = np.stack(train['dx_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# dt_u_train = np.stack(train['dt_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
# velocity_train = np.stack(train['velocity'].values).reshape((-1, 1))
#
#

pde_rnn = PdeRNN(num_t_vals, parameter_units=80, input_units=60, dt=dt, dx=dx, num_parameters=1)
pde_rnn.build(activation='relu')
model = pde_rnn.model_w_loss

if fit:
    model.compile(optimizer=optimizers.Adam(lr=0.001))
    history = model.fit(
        [train['parameters'],
         train['t_values'],
         train['dt_u'],
         train['dx_u'],
         train['x_values']],
        validation_data=(
            [test['parameters'],
             test['t_values'],
             test['dt_u'],
             test['dx_u'],
             test['x_values']], None),
        batch_size=100,
        epochs=500,
        shuffle=True,
        verbose=2)
    model.save_weights('results/withu0x/weights_{}.h5'.format(file_suffix))
    np.save('results/withu0x/history_.npy'.format(file_suffix), history.history)
else:
    model.load_weights('results/withu0x/weights_{}.h5'.format(file_suffix))
    pde_rnn.evaluate(3, test_dict)
    pde_rnn.evaluate(3, train_dict)
    # history = np.load('results/withu0x/history_total.npy').item()
    # plt.figure()
    # plt.plot(np.arange(len(history['loss']))[::20], history['loss'][::20], label='loss')
    # plt.plot(np.arange(len(history['val_loss']))[::20], history['val_loss'][::20], label='validation loss')
    # plt.legend()
    # plt.show()
    # out = pde_rnn.predict_nn_function(np.linspace(0, 2, 200), 0, t_vals)
    # pde_rnn.plot_nn_function(np.linspace(0, 2, 200), 0.5, t_vals)
