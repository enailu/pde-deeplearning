from sympy import symbols, Lambda, Piecewise, integrate, And, diff, sin, lambdify, pi
from sympy.plotting import plot
from sympy.abc import x
import matplotlib.pyplot as plt
import numpy as np

p_lbd = 1.3
p_nu = -4
p_eps = 0.004
dt = 0.001

num_elements = 300
grid = range(-num_elements // 2, num_elements // 2)
h = 2. / num_elements
xvals = np.linspace(-1, 1, num_elements + 1)[:-1]

basis_function_center = Piecewise(((x + h) / h, And(- h <= x, x <= 0)), ((h - x) / h, And(0 <= x, x <= h)), (0, True))

basis_function_right = basis_function_center.subs(x, x - h)
basis_function_left = basis_function_center.subs(x, x + h)

# integrate basis function
cc = float(integrate(basis_function_center ** 2, (x, -1, 1)))
cl = float(integrate(basis_function_center * basis_function_left, (x, -1, 1)))
cr = cl

# integrate basis function with derivative u*u_x
ccxc = float(integrate(basis_function_center * diff(basis_function_center, x) * basis_function_center, (x, -1, 1)))
ccxl = float(integrate(basis_function_center * diff(basis_function_center, x) * basis_function_left, (x, -1, 1)))
ccxr = float(integrate(basis_function_center * diff(basis_function_center, x) * basis_function_right, (x, -1, 1)))

# integrate basis function derivatives
cxcx = float(integrate(diff(basis_function_center, x) ** 2, (x, -1, 1)))

cxlx = float(integrate(diff(basis_function_center, x) * diff(basis_function_left, x), (x, -1, 1)))

cxrx = cxlx


def make_periodic(mat):
    mat[0, -1] = mat[-1, -2]
    mat[-1, 0] = mat[0, 1]
    return mat


# construct basis matrix
A = np.eye(num_elements) * cc
A += np.eye(num_elements, k=1) * cr
A += np.eye(num_elements, k=-1) * cl
# A[0, 0] *= 1 / 2
# A[-1, -1] *= 1 / 2
A_per = make_periodic(A)

# calculate coefficients for u0
z0 = p_lbd - np.sin(np.pi * xvals)



# stiffness matrix
B = np.eye(num_elements) * ccxc
B += np.eye(num_elements, k=-1) * ccxl
B += np.eye(num_elements, k=1) * ccxr
B = make_periodic(B)
C = np.eye(num_elements) * cxcx
C += np.eye(num_elements, k=-1) * cxlx
C += np.eye(num_elements, k=1) * cxrx
C = make_periodic(C)

S = A_per - dt*p_nu * B - dt *p_eps*C
S_bar = A_per
A_bar = A_per +  dt*p_eps*C +  dt*p_nu * B
# S[0, 0] = 1 / 2 * cc - dt * p_eps * cxcx_right_half - dt * p_nu * ccxc_right_half
# S[-1, -1] = 1 / 2 * cc - dt * p_eps * cxcx_left_half - dt * p_nu * ccxc_left_half


z_list = [z0]
# integrate over time
z = z0
for i in range(100):
    right_side = np.matmul(S, z)
    z, _, _, _ = np.linalg.lstsq(A_per, right_side, rcond=None)
    z_list.append(z)

z = z0
z_list2 = [z0]
for i in range(100):
    right_side = np.matmul(S_bar, z)
    z, _, _, _ = np.linalg.lstsq(A_bar, right_side, rcond=None)
    z_list2.append(z)


plt.plot(np.asarray(z_list2).T[:,::10])
plt.show()