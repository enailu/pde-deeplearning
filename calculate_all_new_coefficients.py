import postprocessing
import pandas as pd
import numpy as np
from nn_architectures import PdeRNN
from tqdm import tqdm

# load data
dataset1 = pd.DataFrame(np.load('data/parameters1_precise.npy').item())
dataset2 = pd.DataFrame(np.load('data/parameters2_precise.npy').item())
dataset3 = pd.DataFrame(np.load('data/parameters3_precise.npy').item()).drop('c', 1)
dataset = pd.concat([dataset1, dataset3, dataset2]).to_dict(orient='list')

# extract some things from data
num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
t_vals = np.asarray(dataset['t_values'][0])
x_vals = np.asarray(dataset['x_values'][0])
dx = x_vals[1] - x_vals[0]
dt = t_vals[1] - t_vals[0]

# prepare data: make everything have shape (num_x_vals, num_t_vals, 1)
t_vals_broadcasted = np.broadcast_to(np.asarray(t_vals)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1))
solutions = [np.asarray(solution).T[:, :, np.newaxis] for solution in dataset['solution']]

# specify which weights to load
file_suffix = 'precise'

# build rnn and load weights
pde_rnn = PdeRNN(num_t_vals, parameter_units=200, input_units=60, dt=dt, dx=dx, num_parameters=0,
                 num_parameters_depending_on_t=1)
pde_rnn.build(activation='relu')
model = pde_rnn.model
model.load_weights(f'results/withutx/weights_{file_suffix}.h5')

# calculate new coefficients for all elements in dataset
phi_list = []
coefficients_list = []
for solution in tqdm(solutions):
    phi = np.asarray(model.predict([solution, t_vals_broadcasted, x_vals]))
    coefficients = postprocessing.calculate_coefficients(solution.T, phi, x_vals)
    phi_list.append(phi)
    coefficients_list.append(coefficients)
