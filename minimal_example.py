import fenics
import matplotlib.pyplot as plt

x_values_list = []
for i in range(5):
    mesh = fenics.IntervalMesh(10, 0, 4)
    x_values = mesh.coordinates()
    print(x_values)
    x_values_list.append(x_values)

print(x_values_list)
