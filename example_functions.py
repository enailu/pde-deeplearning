import numpy as np
import sympy as sp
from sympy.abc import x

from periodic_data_utils import extend_function_sympy, extend_function


def viscous_solution(x_values, t_values, a, b, eps=1):
    return 2 * b * eps * np.pi * np.exp(-eps * np.pi ** 2 * t_values.T) * np.sin(np.pi * x_values) / (
            a + b * np.exp(-eps * np.pi ** 2 * t_values.T) * np.cos(np.pi * x_values))


def gen_func_from_sympy(sympy_func, numpy=False, extended=False, left=0.0, right=1.0, value=0.0):
    numpy_func = sp.lambdify(x, sympy_func, 'numpy')
    if extended:
        if numpy:
            return lambda x: extend_function(numpy_func, left, right, value)(x)
        else:
            return extend_function_sympy(sympy_func, x, left, right, value)
    else:
        if numpy:
            return numpy_func
        else:
            return sympy_func


def numpy_version(initial_func):
    def func_numpy(x, **kwargs):
        kwargs['numpy'] = True
        return initial_func(**kwargs)(x)

    return func_numpy


def initial_func1(a=5, b=1, numpy=False, extended=False, left=0.0, right=1.0, value=0.0, scale=1.0, **kwargs):
    sympy_func = 2 * sp.pi * sp.sin(sp.pi * scale * x) / (a + b * sp.cos(sp.pi * x * scale))
    return gen_func_from_sympy(sympy_func, numpy, extended, left, right, value)


def initial_func2(a=0.1, b=2, numpy=False, extended=False, left=0.0, right=1.0, value=0.0):
    sympy_func = sp.sin(sp.pi * x * a) * (sp.cos(2 * sp.pi * x) - 1) ** b
    return gen_func_from_sympy(sympy_func, numpy, extended, left, right, value)


def initial_func3(a=2, b=1, c=-20, numpy=False, extended=False, left=0.0, right=1.0, value=0.0, scale=1.0, **kwargs):
    sympy_func = sp.sin(sp.pi * x * a * scale) * (sp.cos(2 * sp.pi * x * scale) - 1) ** b / (
                c * sp.log(x * scale - left + sp.E))
    return gen_func_from_sympy(sympy_func, numpy, extended, left, right, value)


def pymor_version(initial_func, **kwargs):
    def func_pymor(x, mu):
        # mu is a pymor Parameter
        for key, value in mu.items():
            kwargs[key] = value.item()
        kwargs['numpy'] = True
        result = initial_func(**kwargs)(x)
        if result.shape[-1] == 1:
            return np.squeeze(result, -1)
        else:
            return result

    return func_pymor
